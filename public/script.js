var Load_Now = 4;
var watcher;

new Vue({
	el:"#app",
	data: {
		total: 0,
		products:[

		],
		cart: [],
		search: "cat",
		lastSearch: "",
		loading: false,
		results:[]
	},
	methods: {
		addToCart: function(product) {
			console.log(product.id);
			this.total += product.price;

			let isFound = false;

			for (let i = 0; i < this.cart.length; i++) {
				if (this.cart[i].id === product.id) {
					this.cart[i].qty += 1;
					isFound = true;
				}
			}
			if (!isFound) {
				this.cart.push({
					id: product.id,
					title: product.title,
					price: product.price,
					qty: 1
				});
			}
		},
		increment: function(product) {
			product.qty += 1;
			this.total += product.price;
		},
		decrement: function(product) {
			product.qty -= 1;
			this.total -= product.price;
			if (product.qty <= 0) {
				let i = this.cart.indexOf(product);
				this.cart.splice(i, 1);
			}
		},
		onSubmit: function () {
			this.products = [];
			this.results = [];
			this.loading = true;
			let path= "/search?q=".concat(this.search);
			this.$http.get(path)
				.then(function(response) {
						this.results = response.body;
						this.lastSearch = this.search;
						this.appendResults();
						this.loading = false;
				});
		},
		appendResults: function() {
			if (this.products.length < this.results.length) {
				var toAppend = this.results.slice(
					this.products.length,
					Load_Now+this.products.length);
				this.products = this.products.concat(toAppend);
			}
		}
	},
	filters: {
		currency: function(price) {
			return "₹ ".concat(price.toFixed(2));
		}
	},
	created: function () {
		this.onSubmit();
	},
	updated: function() {
		let sensor = document.querySelector("#product-list-bottom");
		watcher = scrollMonitor.create(sensor);
		watcher.enterViewport(this.appendResults);
	},
	beforeUpdate: function() {
		if (watcher) {
			watcher.destroy();
			watcher = null;
		}
	}
});

