# Simple Near.Store Shop

Simple Product listing and Add to Cart Code with Vue.js

#### Pre-installation

Ensure [Node.js  >=4](https://nodejs.org/en/download/), [NPM](https://docs.npmjs.com) and [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) are installed on your system
 
#### Installation

1. Download and Install dependencies

    ```
    npm install
    ```
    
2. Start project

    ```
    npm run serve
    ```

3. Your site will be available at *localhost:3000*.

